"use strict";

var gulp = require('gulp'),
concatCss = require('gulp-concat-css'),
autoprefixer = require('gulp-autoprefixer'),
livereload = require('gulp-livereload'),
jade = require('gulp-jade'),
stylus = require('gulp-stylus'),
browserSync = require('browser-sync'),
newer = require('gulp-newer'),
plumber = require('gulp-plumber'),
reload = browserSync.reload;

gulp.task('server', function() {
  browserSync({
    server: {
      baseDir: "public"
    }
  });
});

gulp.task('stylus', function() {
return gulp.src(['assets/app/**/*.styl', '!assets/**/_*.styl'])
  .pipe(stylus())
  .pipe(plumber())
  .on('error', console.error.bind(console))
  .pipe(autoprefixer({
      browsers: ['last 5 versions'],
      cascade: false
    }))
  .pipe(gulp.dest('public/app'))
  .pipe(browserSync.reload({stream: true}));
});

gulp.task('jade', function() {
  return gulp.src(['assets/**/*.jade', '!assets/**/_*.jade'])
    .pipe(jade({
      pretty: true,
      basedir: 'assets'
    }))
  .pipe(plumber())
  .on('error', console.error.bind(console))
  .pipe(gulp.dest('./public/'))
  .pipe(browserSync.reload({stream: true}));
});

// gulp.task('index', function() {
//   return gulp.src('assets/pages/index.jade')
//   .pipe(jade({
//     pretty: true,
//     basedir: 'assets'
//   }))
//   .pipe(plumber())
//   .on('error', console.error.bind(console))
//   .pipe(gulp.dest('public'))
//   .pipe(browserSync.reload({stream: true}));
// });

//Blocks
gulp.task('blocks', ['jade'], function() {
  return gulp.src('./assets/pages/blocks.jade')
  .pipe(jade({
    pretty: true,
    basedir: 'assets'
  }))
  .pipe(plumber())
  .on('error', console.error.bind(console))
  .pipe(gulp.dest('./public/pages/'))
  .pipe(browserSync.reload({stream: true}));
});

gulp.task('images', function() {
  return gulp.src(['assets/img/**', '!assets/img/svg', '!assets/img/png'])
    .pipe(newer('public/img'))
    .pipe(gulp.dest('public/img'))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('_images', function() {
  return gulp.src('assets/_img/**')
    .pipe(newer('public/_img'))
    .pipe(gulp.dest('public/_img'))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('app', function() {
  return gulp.src(['assets/app/vendor/*.*', 'assets/app/vendor/**/*.*'])
    .pipe(newer('public/app/vendor'))
    .pipe(gulp.dest('public/app/vendor'))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('font', function() {
  return gulp.src('assets/font/**')
    .pipe(newer('public/font'))
    .pipe(gulp.dest('public/font'))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('watch', function() {
  gulp.watch('assets/blocks/**/*.styl',['stylus']);
  gulp.watch('assets/blocks/**/*.jade', ['jade']);
  gulp.watch('assets/app/vendor/**/*.*', ['app']);
  gulp.watch('assets/img/**', ['images']);
  gulp.watch('assets/_img/**', ['_images']);
  gulp.watch('assets/font/*', ['font']);
  gulp.watch('assets/app/_*.styl', ['stylus']);
  gulp.watch('assets/pages/*',['blocks']);
  gulp.watch('assets/pages/index.jade', ['index']);
  gulp.watch('assets/pages/_*.jade', ['jade']);
});

gulp.task('default', ['jade', 'stylus', 'app', 'images', 'font', '_images', 'server', 'blocks', 'watch']);