$(document).ready(function() {
	$('input, select').styler();

	var app = {
		initialize: function() {
			this.setUpListeners();
		},

		setUpListeners: function() {
			$('form').on('submit', app.submitForm);
			$('form').on('keydown','input', app.removeError);
		},
		submitForm: function(e) {
			e.preventDefault();

			var form = $(this);
			if(app.validateForm(form) === false) return false;
			$.ajax({
				type: 'POST',
				url: 'send.php',
				data: $('#form').serialize(),
				success: function (data) {
					console.log(data);
					$('#send-success').modal('show');
				},
				error: function (xhr, str) {
					alert('Возникла ошибка: ' + xhr.responseCode);
				}
			});
		},
		validateForm: function(form){
			var inputs = form.find('.phone'),
				valid = true;
			inputs.tooltip('destroy');
			$.each(inputs, function(index, val){
				var input = $(val),
					val = input.val(),
					formGroup = input.parents('.form-group'),
					textError = 'Введите телефон';

				var n = $('.phone').val().match( /\d/g );
				n = n ? n = n.length : 0;


				if(n <= 10 || val.length === 0){
					formGroup.addClass('has-error').removeClass('has-success');
					$('.phone').css({'border': '2px solid #a94442', 'boxShadow': '0 0 10px 2px #a94442'});
					input.tooltip({
						trigger: 'manual',
						placement: 'right',
						title: textError
					}).tooltip('show');
					valid = false
				}else{
					formGroup.addClass('has-success').removeClass('has-error');
				}
			});
			return valid;
		},

		removeError: function () {
			$(this).tooltip('destroy').parents('.form-group').removeClass('has-error');
		}
	}
	app.initialize();

	$('.phone').inputmask({"mask": "+7 (999) 999-9999"});

});